package challengemanager

// Round struct represents a challenge round
type Round struct {
	RoundId       uint64
	PrimaryStat   float64
	SecondaryStat float64
}
