package challengemanager

import (
	"errors"
)

// User struct represents a user in a challenge
type User struct {
	UserId           uint64
	Rounds           map[uint64]*Round
	Username         string
	PrimaryAverage   float64
	SecondaryAverage float64
	Rank             uint32
}

// ErrRoundExists is returned when a round is attempted to be added and already exists
var ErrRoundExists = errors.New("this round has already been added to the user")

// ErrRoundNotExist is returned when a round is attempted to be removed that does not exist
var ErrRoundNotExist = errors.New("user does not have that round")

// AddRound adds a round to a user
//
// This will recompute the user's primary and secondary average
func (user *User) AddRound(round *Round) error {
	_, roundExists := user.Rounds[round.RoundId]
	if roundExists {
		return ErrRoundExists
	}

	roundsPlayed := float64(len(user.Rounds))
	user.PrimaryAverage = ((user.PrimaryAverage * roundsPlayed) + round.PrimaryStat) / (roundsPlayed + 1)
	user.SecondaryAverage = ((user.SecondaryAverage * roundsPlayed) + round.SecondaryStat) / (roundsPlayed + 1)
	user.Rounds[round.RoundId] = round
	return nil
}

// RemoveRound removes a round from a user
//
// This will recompute the user's primary and secondary average
func (user *User) RemoveRound(roundId uint64) error {
	round, roundExists := user.Rounds[roundId]
	if !roundExists {
		return ErrRoundNotExist
	}

	roundsPlayed := float64(len(user.Rounds))
	if roundsPlayed > 1 {
		user.PrimaryAverage = ((user.PrimaryAverage * roundsPlayed) - round.PrimaryStat) / (roundsPlayed - 1)
		user.SecondaryAverage = ((user.SecondaryAverage * roundsPlayed) - round.SecondaryStat) / (roundsPlayed - 1)
	} else {
		user.PrimaryAverage = 0
		user.SecondaryAverage = 0
	}

	delete(user.Rounds, roundId)
	return nil
}
