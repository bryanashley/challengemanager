# ChallengeManager Code Challenge

## Problem

You are creating a "challenges" platform for players to compete on rounds of golf. Each player can submit an arbitrary number of rounds, and each challenge is based on a primary and a tie-breaker stat. For the purposes of this exercise, the value for each stat is the average across all the user's rounds. You want to create a component to efficiently maintain the rank for each user that is a part of a given challenge.

Question:
Create a class that supports the following interface, in whatever language you are comfortable using. It should be an efficient operation to request the user rank after adding a round.

```
class ChallengeManager {
    void addUser(uint64_t userId, std::string username);
    void addUserRound(uint64_t userId, uint64_t roundId, double primaryStat, double secondaryStat);
    void removeUserRound(uint64_t userId, uint64_t roundId);
    uint32_t getUserRank(uint64_t userId);
    std::vector<uint64_t> getUsers();
    std::vector<uint64_t> getUserRounds(uint64_t userId);
}
```

## Solution 

### Data Structure

The data structure I chose consists of three structs, User, Round, and Challenge. 

#### Challenge

| Attribute | Type | Description |
|---|---|---|
| Users | map[uint64]*User | A mapping of all of the challenge's users |

#### User

| Attribute | Type | Description |
|---|---|---|
| UserId | uint64 | User Identifier |
| Username | string | Users username |
| Rounds | map[uint64]*Round | A mapping of all of the user's rounds, key being the round id |
| PrimaryAverage | float64 | The average of all of the user's round's primary stats |
| SecondaryAverage | float64 | The average of all of the user's round's secondary stats |
| Rank | uint32 | The users current rank in their challenge |

#### Round

| Attribute | Type | Description |
|---|---|---|
| RoundId | uint64 | Round Identifier |
| PrimaryStat | float64 | Rounds primary stat |
| SecondaryStat | float64 | Rounds secondary stat |

### Usage

Sample below code, or try it yourself [here](https://play.golang.org/p/OQUqiQ3ehJ)
```go
// Initialize a challenge
challenge := &Challenge{Users: make(map[uint64]*User)}

// Add users to a challenge
challenge.AddUser(1, "Hideki Matsuyama")
challenge.AddUser(2, "Dustin Johnson")
challenge.AddUser(3, "Rory McIlroy")

// Add users rounds
challenge.AddUserRound(1, 1, 10, 10)
challenge.AddUserRound(1, 2, 8, 6)
challenge.AddUserRound(1, 3, 9, 4)

challenge.AddUserRound(2, 1, 9, 4)
challenge.AddUserRound(2, 2, 5, 3)
challenge.AddUserRound(2, 3, 1, 5)

challenge.AddUserRound(3, 1, 2, 16)
challenge.AddUserRound(3, 2, 4, 5)

// Remove users rounds
challenge.RemoveUserRound(1, 2)

// Get a users rank
challenge.GetUserRank(1) 

// Get all user ids 
challenge.GetUsers()

```

### Decisions Made

#### Reranking 
Whenever `AddUserRound` or `RemoveUserRound` is called `AddRound`/`RemoveRound` is called on a user struct which will recompute that user's averages. After `AddRound`/`RemoveRound` are called the challenge itself will call `recalculateRanks`. This method will sort the challenge's users by first PrimaryStatAverage and then by SecondaryStatAverage. This recalculating of averages and then ranks happens whenever a round is added and removed to make user rank lookups the most efficient operation. 

#### Assumptions

Throughout the project there were a few things undefined that I made assumptions on:
  - Players are ranked by lowest average primary and then lowest average secondary stat. I went with lowest as it is most similar to golf :) 
  - Players that have the same primary and secondary stat average are tied at that rank
  - If a user has no rounds played, they will be given a rank of 0, to represent unranked. I figured 0 was a safe rank for unranked as ranking systems are normally 1-indexed. 
  - I strayed a little bit from the required interface to support returning errors (where neccessary) following go's best practices


