package challengemanager

import (
	"errors"
	"sort"
)

// ChallengeManager facilates ranking users after their inputting of many rounds
type ChallengeManager interface {
	AddUser(userId uint64, username string) error
	AddUserRound(userId uint64, roundId uint64, primaryStat float64, secondaryStat float64)
	RemoveUserRound(userId uint64, roundId uint64)
	GetUserRank(userId uint64) (rank uint32)
	GetUsers() []uint64
}

// Challenge Struct stores a map of userIds to their objects
//
// this implements ChallengeManager
type Challenge struct {
	Users map[uint64]*User
}

// ErrUserExists is returned when a user who is already in a challenge is added
var ErrUserExists = errors.New("user already in the challenge")

// ErrUserDoesNotExist is returned when a user is not in a given challenge
var ErrUserDoesNotExist = errors.New("user is not in challenge")

// AddUser a user to a challenge
//
// If the user is already in the challege, this will return an error
func (challenge *Challenge) AddUser(userId uint64, username string) error {
	_, exists := challenge.Users[userId]
	if exists {
		return ErrUserExists
	}

	challenge.Users[userId] = &User{UserId: userId, Username: username, Rounds: make(map[uint64]*Round)}
	return nil
}

// AddUserRound Adds a round for a given user
//
// This returns an error if the user is not in the challenge
// or if the round already exists for the user
func (challenge *Challenge) AddUserRound(userId uint64, roundId uint64, primaryStat float64, secondaryStat float64) error {
	_, userExists := challenge.Users[userId]
	if !userExists {
		return ErrUserDoesNotExist
	}

	user := challenge.Users[userId]
	err := user.AddRound(&Round{RoundId: roundId, PrimaryStat: primaryStat, SecondaryStat: secondaryStat})
	if err != nil {
		return err
	}

	challenge.recalculateRanks()
	return nil
}

// RemoveUserRound Removes a round from a user in the challenge
//
// This returns an error if the user is not in the challenge
// or if the round does not exist for the user
func (challenge *Challenge) RemoveUserRound(userId uint64, roundId uint64) error {
	_, userExists := challenge.Users[userId]
	if !userExists {
		return ErrUserDoesNotExist
	}

	user := challenge.Users[userId]
	err := user.RemoveRound(roundId)
	if err != nil {
		return err
	}

	challenge.recalculateRanks()
	return nil
}

// GetUsers Returns an array of all userIds in a challenge
func (challenge *Challenge) GetUsers() []uint64 {
	userIds := make([]uint64, 0, len(challenge.Users))
	for userId := range challenge.Users {
		userIds = append(userIds, userId)
	}

	return userIds
}

// GetUserRank Returns the rank of a given user
//
// This returns an error if the user is not in the challenge
// This returns 0 for users that are unranked (have 0 rounds input)
func (challenge *Challenge) GetUserRank(userId uint64) (*uint32, error) {
	user, userExists := challenge.Users[userId]
	if !userExists {
		return nil, ErrUserDoesNotExist
	}

	return &user.Rank, nil
}

// recalculateRanks Reranks all users in a challenge
//
// This is called whenever a round is added or removed to
// a user in a challenge. This is to have requesting of a rank
// be most efficient
func (challenge *Challenge) recalculateRanks() {
	userCount := len(challenge.Users)
	userSlice := make([]*User, userCount)
	i := 0
	for _, user := range challenge.Users {
		userSlice[i] = user
		i++
	}

	sort.Slice(userSlice, func(i, j int) bool {
		if userSlice[i].PrimaryAverage == userSlice[j].PrimaryAverage {
			return userSlice[i].SecondaryAverage < userSlice[j].SecondaryAverage
		}

		return userSlice[i].PrimaryAverage < userSlice[j].PrimaryAverage
	})

	// rank starts at 0, because users with primary and secondary averages of 0
	// have 0 rounds played and therefore are unranked (Rank of 0)
	rank := 0
	for index, user := range userSlice {
		if rank == 0 && user.PrimaryAverage > 0 {
			rank++
		}

		user.Rank = uint32(rank)
		if index < userCount-1 {
			nextUser := userSlice[index+1]
			if !(user.PrimaryAverage == nextUser.PrimaryAverage && user.SecondaryAverage == nextUser.SecondaryAverage) {
				rank++
			}
		}
	}
}
