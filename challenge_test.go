package challengemanager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bryanashley/challengemanager"
)

func TestAddUser(t *testing.T) {
	c := &challengemanager.Challenge{Users: make(map[uint64]*challengemanager.User)}

	t.Log("tests add user adds user to challenge")
	assert.NoError(t, c.AddUser(1, "Tiger"))
	assert.Equal(t, 1, len(c.Users))

	t.Log("tests add user that is already added errors")
	assert.Error(t, c.AddUser(1, "Tiger"))
	assert.Equal(t, 1, len(c.Users))
}

func TestAddUserRound(t *testing.T) {
	c := &challengemanager.Challenge{Users: make(map[uint64]*challengemanager.User)}
	c.AddUser(1, "Tiger")

	t.Log("tests add user round adds a round to a user in the challenge and ranks them")
	assert.NoError(t, c.AddUserRound(1, 1, 10, 5))
	assert.Equal(t, 1, len(c.Users[1].Rounds))
	assert.Equal(t, uint32(1), c.Users[1].Rank)

	t.Log("tests add user round returns error when user not in challenge")
	assert.Error(t, c.AddUserRound(15, 1, 10, 5))
}

func TestRemoveUserRound(t *testing.T) {
	c := &challengemanager.Challenge{Users: make(map[uint64]*challengemanager.User)}
	c.AddUser(1, "Tiger")
	c.AddUserRound(1, 1, 10, 5)
	c.AddUser(2, "Rory")
	c.AddUserRound(2, 1, 8, 4)

	t.Log("tests remove user round removes a users round and reranks them")
	assert.Equal(t, uint32(1), c.Users[2].Rank)
	assert.NoError(t, c.RemoveUserRound(2, 1))
	// Rank should be 0 as user 2 is no longer ranked (has 0 rounds)
	assert.Equal(t, uint32(0), c.Users[2].Rank)
}
