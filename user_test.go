package challengemanager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bryanashley/challengemanager"
)

func TestAddRound(t *testing.T) {
	u := &challengemanager.User{UserId: 1, Rounds: make(map[uint64]*challengemanager.Round)}
	r1 := &challengemanager.Round{RoundId: 1, PrimaryStat: 10, SecondaryStat: 10}

	t.Log("testing add round succeeds and averages are set")
	assert.NoError(t, u.AddRound(r1))
	assert.Equal(t, float64(10), u.PrimaryAverage)
	assert.Equal(t, float64(10), u.SecondaryAverage)

	t.Log("testing add same round again errors with ErrRoundExists")
	err1 := u.AddRound(r1)
	assert.Error(t, err1)

	t.Log("testing adding another round adjusts averages")
	r2 := &challengemanager.Round{RoundId: 2, PrimaryStat: 15, SecondaryStat: 5}
	assert.NoError(t, u.AddRound(r2))
	assert.Equal(t, float64(12.5), u.PrimaryAverage)
	assert.Equal(t, float64(7.5), u.SecondaryAverage)
}

func TestRemoveRound(t *testing.T) {
	u := &challengemanager.User{UserId: 1, Rounds: make(map[uint64]*challengemanager.Round)}

	t.Log("testing remove round that doesn't exist returns ErrRoundNotExist")
	assert.Error(t, u.RemoveRound(1))

	t.Log("testing remove round removes the round and adjusts averages")
	r1 := &challengemanager.Round{RoundId: 1, PrimaryStat: 10, SecondaryStat: 10}
	u.AddRound(r1)
	assert.NoError(t, u.RemoveRound(r1.RoundId))
	assert.Equal(t, float64(0), u.PrimaryAverage)
	assert.Empty(t, u.Rounds)
}
